import { Item } from './item';

export interface MoveData {
	columnFromName: string;
	columnFromItems: Item[];
	columnToName: string;
	columnToItems: Item[];
}
