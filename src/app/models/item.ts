export interface Item {
  id?: string,
  columnName: string,
  content: string
}
