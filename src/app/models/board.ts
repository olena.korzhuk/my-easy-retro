import { Column } from './column.model';

export interface Board {
	_id: string;
	name: string;
	created_date?: string;
	columns: Column[];
}
