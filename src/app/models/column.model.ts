import { Item } from './item';

export interface Column {
	_id: string;
	name: string;
	items: Item[];
	color: string;
}
