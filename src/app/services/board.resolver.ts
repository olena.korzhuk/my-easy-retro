import { Injectable } from '@angular/core';
import {
	Router,
	Resolve,
	RouterStateSnapshot,
	ActivatedRouteSnapshot,
} from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { Board } from '../models/board';
import { BoardsService } from './boards.service';
import { catchError } from 'rxjs/operators';

@Injectable({
	providedIn: 'root',
})
export class BoardResolver implements Resolve<Board> {
	constructor(private boardsService: BoardsService, private router: Router) {}

	resolve(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<Board> {
		return this.boardsService.getBoard(route.params['id']).pipe(
			catchError(() => {
				this.router.navigate(['']);
				return EMPTY;
			})
		);
	}
}
