import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Board } from '../models/board';
import { delay } from 'rxjs/operators';
import { Item } from '../models/item';

@Injectable({
	providedIn: 'root',
})
export class BoardsService {
	constructor(private httpService: HttpClient) {}

	boardAdded: EventEmitter<string> = new EventEmitter<string>();

	getBoards(): Observable<Board[]> {
		// For testing purposes only
		delay(5000);
		return this.httpService.get<Board[]>('/api/boards');
	}

	getBoard(boardId: string): Observable<Board> {
		return this.httpService.get<Board>(`/api/boards/${boardId}`);
	}

	createBoard(boardData: Board) {
		return this.httpService.post<Board>('/api/boards', boardData);
	}

	addBoardColumnItem(boardId: string, columnId: string, columnItem: Item) {
		console.log('In Http service', boardId, columnId, columnItem);
		return this.httpService
			.put<Item>(`/api/boards/${boardId}`, columnItem)
			.subscribe((res) => console.log('RESPONSE', res));
	}
}
