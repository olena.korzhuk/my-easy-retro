import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Board } from '../models/board';
import { BoardsService } from './boards.service';
import { finalize, tap } from 'rxjs/operators';

@Injectable({
	providedIn: 'root',
})
export class BoardsStoreService {
	private isLoading$$ = new BehaviorSubject<boolean>(false);
	private boards$$ = new BehaviorSubject<Board[]>([]);

	readonly isLoading$: Observable<boolean> = this.isLoading$$.asObservable();
	readonly boards$: Observable<Board[]> = this.boards$$.asObservable();

	constructor(private boardsHttpService: BoardsService) {}

	loadBoards() {
		this.isLoading$$.next(true);
		return this.boardsHttpService
			.getBoards()
			.pipe(
				tap((boards) => this.boards$$.next(boards)),
				finalize(() => {
					this.isLoading$$.next(false);
					console.log('In finalize', this.isLoading$$.value);
				})
			)
			.subscribe();
	}
}
