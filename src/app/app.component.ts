import { Component, OnInit } from '@angular/core';
import { LoadingService } from './components/loading/loading.service';
import { BoardsStoreService } from './services/boards.store.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	providers: [LoadingService],
})
export class AppComponent implements OnInit {
	title = 'my-easy-retro';

	constructor(private boardsStoreService: BoardsStoreService) {}

	ngOnInit() {
		this.boardsStoreService.loadBoards();
	}
}
