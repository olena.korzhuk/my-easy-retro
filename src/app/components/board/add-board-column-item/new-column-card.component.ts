import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Column } from '../../../models/column.model';
import { BoardsService } from '../../../services/boards.service';

@Component({
	selector: 'app-new-column-item',
	styleUrls: ['new-column-card.component.scss'],
	templateUrl: 'new-column-card.component.html',
})
export class NewColumnCard implements OnInit {
	@Input() column: Column = {} as Column;
	@Input() boardId: string = '';
	itemValue = '';

	ngOnInit() {
		console.log('Column Id in New Column Card component', this.column._id);
	}

	addBoardColumnCard: FormGroup = this.fb.group({
		items: this.fb.array([]),
	});

	constructor(private fb: FormBuilder, private boardsService: BoardsService) {}

	get items() {
		return this.addBoardColumnCard?.controls['items'] as FormArray;
	}

	onAddNewColumnCard() {
		const itemForm = this.fb.group({
			itemContent: [''],
		});
		this.items.push(itemForm);
	}

	onCancelAddCard(addItemIndex: number) {
		this.items.removeAt(addItemIndex);
	}

	onAddCardSubmit(itemText: string, addItemIndex: number) {
		console.log('column items', this.column.items);
		console.log('item text', itemText);
		if (itemText) {
			this.column.items.push({
				id: this.column._id,
				columnName: this.column.name,
				content: itemText,
			});
			this.boardsService.addBoardColumnItem(this.boardId, this.column._id, {
				columnName: this.column.name,
				content: itemText,
			});
		}
		this.items.removeAt(addItemIndex);
		// this.showAddInput = false;
	}

	onSubmit(event: any) {
		console.log(event);
	}
}
