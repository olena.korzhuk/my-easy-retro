import { Component, OnInit } from '@angular/core';
import { Column } from '../../models/column.model';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Board } from '../../models/board';

@Component({
	selector: 'app-board',
	templateUrl: './board.component.html',
	styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {
	board$!: Observable<Board>;
	columns: Column[] = [
		{
			_id: '0',
			name: 'Todo',
			items: [
				{
					id: '0',
					columnName: 'Todo',
					content: 'Todo 1',
				},
				{
					id: '1',
					columnName: 'Todo',
					content: 'Todo 2',
				},
				{
					id: '2',
					columnName: 'Todo',
					content: 'Todo 3',
				},
			],
			color: '#009886',
		},
		{
			_id: '3',
			name: 'Stop',
			items: [
				{
					id: '3',
					columnName: 'Stop',
					content: 'Stop 1',
				},
				{
					id: '4',
					columnName: 'Stop',
					content: 'Stop 2',
				},
				{
					id: '5',
					columnName: 'Stop',
					content: 'Stop 3',
				},
			],
			color: '#e92c64',
		},
		{
			_id: '2',
			name: 'Continue',
			items: [
				{
					id: '6',
					columnName: 'Continue',
					content: 'Continue 1',
				},
				{
					id: '7',
					columnName: 'Continue',
					content: 'Continue 2',
				},
				{
					id: '8',
					columnName: 'Continue',
					content: 'Continue 3',
				},
			],
			color: '#A63EB9',
		},
	];

	constructor(private activatedRoute: ActivatedRoute) {}

	ngOnInit() {
		this.board$ = this.activatedRoute.data.pipe(map((data) => data.board));
	}
}
