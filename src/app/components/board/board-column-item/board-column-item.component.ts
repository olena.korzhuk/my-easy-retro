import { Component } from '@angular/core';

@Component({
	selector: 'app-board-column-item',
	styleUrls: ['board-column-item.component.scss'],
	templateUrl: 'board-column-item.component.html',
})
export class BoardColumnItemComponent {}
