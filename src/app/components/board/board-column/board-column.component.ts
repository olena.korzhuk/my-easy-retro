import {
	AfterViewInit,
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnInit,
	Output,
	ViewChild,
} from '@angular/core';
import {
	CdkDragDrop,
	moveItemInArray,
	transferArrayItem,
} from '@angular/cdk/drag-drop';
import { Column } from '../../../models/column.model';
import { MoveData } from '../../../models/move-data.model';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Item } from '../../../models/item';

@Component({
	selector: 'app-board-column',
	templateUrl: './board-column.component.html',
	styleUrls: ['./board-column.component.scss'],
})
export class BoardColumnComponent implements OnInit, AfterViewInit {
	@Input() column: Column = {} as Column;
	@Input() boardId: string = '';
	@Output() itemMoved: EventEmitter<MoveData> = new EventEmitter<MoveData>();
	@ViewChild('columnHeader') columnHeaderVar: ElementRef | undefined;

	addNewItemForm: FormGroup = this.fb.group({
		items: this.fb.array([]),
	});

	constructor(private fb: FormBuilder) {}

	ngOnInit(): void {
		console.log(
			'Column in Board Column Component',
			this.column,
			this.column._id
		);
	}

	ngAfterViewInit() {}

	get items() {
		return this.addNewItemForm?.controls['items'] as FormArray;
	}

	onAddItem() {
		const itemForm = this.fb.group({
			itemContent: [''],
		});
		this.items.push(itemForm);
	}

	onCancelAddItem(addItemIndex: number) {
		this.items.removeAt(addItemIndex);
	}

	onAddItemSubmit(itemText: string, addItemIndex: number) {
		if (itemText) {
			this.column.items.push({
				id: 'new_item_123',
				columnName: this.column.name,
				content: itemText,
			});
		}
		this.items.removeAt(addItemIndex);
		// this.showAddInput = false;
	}

	// onAddItemClick() {
	//   this.showAddInput = !this.showAddInput
	// }

	// onCloseAddInput() {
	//   this.showAddInput = false;
	// }

	drop(event: CdkDragDrop<Item[]>) {
		if (event.previousContainer === event.container) {
			moveItemInArray(
				event.container.data,
				event.previousIndex,
				event.currentIndex
			);
		} else {
			transferArrayItem(
				event.previousContainer.data,
				event.container.data,
				event.previousIndex,
				event.currentIndex
			);
			const columnFromName = event.previousContainer.data[0].columnName;
			console.log(
				event.previousContainer.element.nativeElement.childNodes.item(0)
					.textContent
			);
			const columnToName = event.container.data[0].columnName;
			console.log(columnToName);
			this.itemMoved.emit({
				columnFromName,
				columnFromItems: event.previousContainer.data,
				columnToName,
				columnToItems: event.container.data,
			});
		}
	}
}
