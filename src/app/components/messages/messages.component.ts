import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MessagesService } from './messages.service';
import { tap } from 'rxjs/operators';

@Component({
	selector: 'app-messages',
	templateUrl: './messages.component.html',
	styleUrls: ['./messages.component.scss'],
})
export class MessagesComponent implements OnInit {
	showMessages = false;
	errors$: Observable<string[]>;

	constructor(public messagesService: MessagesService) {
		this.errors$ = this.messagesService.errors$.pipe(
			tap(() => (this.showMessages = true))
		);
	}

	ngOnInit(): void {}

	onClose() {
		this.showMessages = false;
	}
}
