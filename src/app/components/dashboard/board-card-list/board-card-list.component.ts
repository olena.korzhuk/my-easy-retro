import { Component, Input } from '@angular/core';
import { Board } from '../../../models/board';

@Component({
	selector: 'app-board-card-list',
	templateUrl: './board-card-list.component.html',
	styleUrls: ['./board-card-list.component.scss'],
})
export class BoardCardListComponent {
	@Input() boards!: Board[];
}
