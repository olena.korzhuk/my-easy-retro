import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadingService } from '../../loading/loading.service';
import { BoardsService } from '../../../services/boards.service';
import { Board } from '../../../models/board';

@Component({
	selector: 'app-add-board1-dialog',
	templateUrl: './add-board-dialog.component.html',
	styleUrls: ['./add-board-dialog.component.scss'],
	providers: [LoadingService],
})
export class AddBoardDialogComponent implements OnInit {
	boardForm = new FormGroup({
		name: new FormControl(null, Validators.required),
		columnOne: new FormControl('Start'),
		columnTwo: new FormControl('Stop'),
		columnThree: new FormControl('Continue'),
	});

	constructor(
		public dialogRef: MatDialogRef<AddBoardDialogComponent>,
		private boardService: BoardsService,
		private loadingService: LoadingService
	) {}

	ngOnInit(): void {}

	onSubmit() {
		const board: Board = {
			_id: '',
			name: this.boardForm.value.name,
			columns: [
				{
					_id: '',
					name: this.boardForm.value.columnOne,
					color: 'green',
					items: [],
				},
				{
					_id: '',
					name: this.boardForm.value.columnTwo,
					color: 'red',
					items: [],
				},
				{
					_id: '',
					name: this.boardForm.value.columnThree,
					color: 'violet',
					items: [],
				},
			],
		};

		const createBoard$ = this.boardService.createBoard(board);

		this.loadingService
			.showLoaderUntilCompleted(createBoard$)
			.subscribe((val) => {
				this.dialogRef.close(val);
			});
		// this.boardService.createBoard(board1)
		//   .subscribe(val => {
		//     this.boardForm.reset();
		//     this.dialogRef.close(val);
		//   })
	}

	save() {
		const changes = this.boardForm.value;
		console.log(this.boardForm.value);

		this.dialogRef.close(changes);
	}

	close() {
		this.dialogRef.close();
	}

	cancelCreate() {
		this.close();
	}
}
