import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddBoardDialogComponent } from './add-board-dialog/add-board-dialog.component';
import { Observable, Subscription } from 'rxjs';
import { filter, tap } from 'rxjs/operators';
import { LoadingService } from '../loading/loading.service';
import { Board } from '../../models/board';
import { BoardsService } from '../../services/boards.service';
import { BoardsStoreService } from '../../services/boards.store.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
	boards$!: Observable<Board[]>;
	isLoading$!: Observable<boolean>;
	boardAddedSubscription!: Subscription;

	constructor(
		private boardService: BoardsService,
		private dialog: MatDialog,
		private loadingService: LoadingService,
		private boardsStoreService: BoardsStoreService
	) {
		this.boardsStoreService.loadBoards();
	}

	ngOnInit() {
		// this.loadBoards();
		// this.boardsStoreService.loadBoards();
		this.boards$ = this.boardsStoreService.boards$;
		this.isLoading$ = this.boardsStoreService.isLoading$;

		// this.boardAddedSubscription = this.boardService.boardAdded.subscribe(() => {
		// 	this.boards$ = this.loadBoards();
		// });
	}

	// loadBoards(): Observable<Board[]> {
	// 	const loadBoards$ = this.boardService.getBoards();
	// 	return this.loadingService.showLoaderUntilCompleted(loadBoards$);
	// }

	ngOnDestroy() {
		this.boardAddedSubscription?.unsubscribe();
	}

	openAddBoardDialog() {
		const dialogConfig = new MatDialogConfig();

		dialogConfig.autoFocus = true;
		dialogConfig.width = '400px';
		const dialogRef = this.dialog.open(AddBoardDialogComponent, dialogConfig);

		dialogRef
			.afterClosed()
			.pipe(
				filter((val) => !!val),
				tap(() => this.boardService.boardAdded.emit('Board created!'))
			)
			.subscribe();
	}
}
