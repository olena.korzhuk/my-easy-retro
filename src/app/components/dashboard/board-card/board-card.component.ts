import { Component, Input, OnInit } from '@angular/core';
import { Board } from '../../../models/board';

@Component({
	selector: 'app-board-card',
	templateUrl: './board-card.component.html',
	styleUrls: ['./board-card.component.scss'],
})
export class BoardCardComponent implements OnInit {
	@Input() board: Board = {} as Board;

	constructor() {}

	ngOnInit(): void {
		this.board.columns;
	}
}
