import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatChipsModule } from '@angular/material/chips';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {
	MatProgressSpinnerModule,
} from '@angular/material/progress-spinner';
import { AddBoardDialogComponent } from './components/dashboard/add-board-dialog/add-board-dialog.component';
import { BoardCardComponent } from './components/dashboard/board-card/board-card.component';
import { BoardComponent } from './components/board/board.component';
import { HeaderComponent } from './components/header/header.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { MessagesComponent } from './components/messages/messages.component';
import { BoardColumnComponent } from './components/board/board-column/board-column.component';
import { LoadingComponent } from './components/loading/loading.component';
import { BoardCardListComponent } from './components/dashboard/board-card-list/board-card-list.component';
import { BoardColumnItemComponent } from './components/board/board-column-item/board-column-item.component';
import { NewColumnCard } from './components/board/add-board-column-item/new-column-card.component';

@NgModule({
	declarations: [
		AddBoardDialogComponent,
		AppComponent,
		BoardCardComponent,
		BoardComponent,
		BoardColumnComponent,
		NewColumnCard,
		BoardColumnItemComponent,
		DashboardComponent,
		HeaderComponent,
		PageNotFoundComponent,
		BoardCardListComponent,
		LoadingComponent,
		MessagesComponent,
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,
		ReactiveFormsModule,
		AppRoutingModule,
		MatCardModule,
		MatButtonModule,
		MatIconModule,
		MatGridListModule,
		MatMenuModule,
		MatProgressSpinnerModule,
		MatChipsModule,
		MatToolbarModule,
		MatInputModule,
		MatDialogModule,
		DragDropModule,
		MatFormFieldModule,
		MatProgressSpinnerModule,
	],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
