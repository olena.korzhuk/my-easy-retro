import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BoardResolver } from './services/board.resolver';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { BoardComponent } from './components/board/board.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

const routes: Routes = [
	{
		path: '',
		redirectTo: '/dashboard',
		pathMatch: 'full',
	},
	{
		path: 'dashboard',
		component: DashboardComponent,
	},
	{
		path: 'board/:id',
		component: BoardComponent,
		resolve: {
			board: BoardResolver,
		},
	},
	{
		path: 'not-found',
		component: PageNotFoundComponent,
	},
	{
		path: '**',
		redirectTo: '/not-found',
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
