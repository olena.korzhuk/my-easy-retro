export interface Board {
  id: string,
  name: string,
  columns: Column[],
  createdAt?: Date,
}

export interface Column {
  id: string,
  name: string,
  color: Colors,
}

enum Colors {
  RED = 'Red',
  GREEN = 'Green',
  VIOLET = 'Violet'
}

export const Boards: Board[] = [
  {
    id: 'utr1',
    name: 'My retro board',
    columns: [
      {
        id: 'col123',
        color: Colors.GREEN,
        name: 'TODO'
      },
      {
        id: 'col345',
        color: Colors.RED,
        name: 'STOP'
      },
      {
        id: 'col678',
        color: Colors.VIOLET,
        name: 'Continue'
      }
    ]
  },
  {
    id: 'utr2',
    name: 'My retro board 2',
    columns: [
      {
        id: 'col1234',
        color: Colors.GREEN,
        name: 'TODO'
      },
      {
        id: 'col3454',
        color: Colors.RED,
        name: 'STOP'
      },
      {
        id: 'col6783',
        color: Colors.VIOLET,
        name: 'Continue'
      }
    ]
  },
];
