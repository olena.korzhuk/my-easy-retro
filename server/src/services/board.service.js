const Board = require('../models/Board');
const { CustomError } = require('../utils/CustomError');

// const createUserBoard = async (userId, loadPayload) => {
const createUserBoard = async (boardPayload) => {
	const boardDoc = {
		...boardPayload,
		// created_by: userId,
		created_date: new Date().toISOString(),
	};
	try {
		await Board.create(boardDoc);
	} catch (e) {
		throw new CustomError(500, 'Server Error: could not create load');
	}
};

// const getUserBoards = async (userId) => {
const getUserBoards = async () => {
	const boards = await Board.find({
		// assigned_to: userId,
		// status: LOAD_STATUSES.ASSIGNED
	});
	return boards;
};

// const getUserLoad = async (userId, loadId, userRole) => {
const getUserBoard = async (boardId) => {
	let foundBoard;
	try {
		foundBoard = await Board.findOne({
			_id: boardId,
		});
		return foundBoard;
	} catch (e) {
		throw new CustomError(500, 'Get Load: could not find load');
	}
};

// const updateUSerLoad = async (loadId, payload) => {
const updateUserBoard = async (boardId, payload) => {
	let board = await Board.findOne({ _id: boardId });

	// if (!boardId) {
	// 	throw new CustomError(400, 'Could not find load');
	// }
	//
	// try {
	// 	await Board.findOneAndUpdate({ _id: boardId }, payload).select('-__v');
	// } catch (e) {
	// 	throw new CustomError(500, 'Could not update board1');
	// }
};

const createBoardColumnItem = async (boardId, payload) => {
	console.log('Board in be service', boardId);
	console.log('finding board');
	// let board = await Board.findOne({ _id: 1 });

	console.log('Payload in be serivce', payload);

	// if (!boardId) {
	throw new CustomError(400, 'Could not find load');
	// }

	// try {
	// 	await Board.findOneAndUpdate({ _id: boardId }, payload).select('-__v');
	// } catch (e) {
	// 	throw new CustomError(500, 'Could not update board1');
	// }
};

// const archiveBoard = async (userId, loadId) => {
const archiveBoard = async (boardId) => {
	// let load = await Board.findOne({created_by: userId, _id: loadId}).select('-__v');
	let load = await Board.findOne({ _id: boardId }).select('-__v');

	if (!load) {
		throw new CustomError(500, 'Could not find load');
	}

	try {
		await Board.deleteOne({ _id: boardId });
	} catch (e) {
		return new CustomError(500, 'Delete board1: Internal server error');
	}
};

module.exports = {
	createUserBoard,
	getUserBoards,
	getUserBoard,
	updateUserBoard,
	archiveBoard,
	createBoardColumnItem,
};
