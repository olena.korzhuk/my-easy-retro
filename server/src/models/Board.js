const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({
  content: {
    type: String
  }
});

const columnSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  color: {
    type: String
  },
  items: {
    type: [itemSchema]
  }
});

const boardSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  columns: {
    type: [columnSchema]
  },
  created_date: {
    type: String
  },
});

const Board = mongoose.model('board', boardSchema);

module.exports = Board;
