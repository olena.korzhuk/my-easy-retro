const handleClientValidationError = (res, e) => {
	res.status(400).json({
		message: `Validation error: ${e.message}`,
	});
};

module.exports.getAuthValidateMiddleware = (schema) => {
	return async function (req, res, next) {
		try {
			await schema.validateAsync(req.body);
			next();
		} catch (e) {
			handleClientValidationError(res, e);
		}
	};
};
