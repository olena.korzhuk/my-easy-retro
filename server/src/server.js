const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const app = express();
const boardRoutes = require('./routes/boards.routes');
const authRoutes = require('./routes/auth.routes');
require('dotenv').config();
const port = process.env.PORT || 8080;
const uri = process.env.DB_URI;

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(morgan('tiny'));
// app.use(express.static(path.join(__dirname, 'build')));

const connectToDb = async (uri) => {
	try {
		await mongoose.connect(uri);
		console.log('CONNECTED TO MONGO');
	} catch (e) {
		console.error('Failed to connect to MongoDB', e);
	}
};

app.use('/api/auth', authRoutes);
app.use('/api/boards', boardRoutes);

connectToDb(uri).then(() =>
	app.listen(port, () =>
		console.log(`Server is up and running on port ${port}`)
	)
);
