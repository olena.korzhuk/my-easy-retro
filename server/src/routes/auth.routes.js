const { Router } = require('express');
const authController = require('./../controllers/auth.controller');
const { registerSchema } = require('./../validators/auth.validator');
const {
	getAuthValidateMiddleware,
} = require('./../middleware/auth.middleware');
const { loginSchema } = require('../validators/auth.validator');

const router = Router();

router.post(
	'/register',
	getAuthValidateMiddleware(registerSchema),
	authController.register
);

router.post(
	'/login',
	getAuthValidateMiddleware(loginSchema),
	authController.login
);

module.exports = router;
