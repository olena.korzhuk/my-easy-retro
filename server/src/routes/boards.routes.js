const { Router } = require('express');
// const { getValidateMiddleware } = require('./../middleware/auth.middleware');
const boardController = require('../controllers/boards.controller');

const router = Router();

router.get('/', boardController.getBoards);
router.get('/:boardId', boardController.getBoard);
// router.post('/', boardController.addBoard);
router.put('/:boardId', boardController.addColumnItem);
// router.put('/:boardId', boardController.updateBoard);
router.delete('/:boardId', boardController.deleteBoard);

module.exports = router;
