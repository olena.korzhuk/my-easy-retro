const boardService = require('../services/board.service');

module.exports.getBoards = async (req, res) => {
	// const {id: userId, role} = req.user;

	try {
		const boards = await boardService.getUserBoards();
		setTimeout(() => {
			res.status(200).json(boards);
		}, 2000);
	} catch (e) {
		if (e.status) {
			res.status(e.status).json({ message: e.message });
		} else {
			return res
				.status(500)
				.json({ message: 'Get board1: Internal server error' });
		}
	}
};

module.exports.addBoard = async (req, res) => {
	// const {id: userId} = req.user;
	const board = req.body;

	try {
		// await createUserBoard(userId, load);
		await boardService.createUserBoard(board);
		res.status(200).json({ message: 'Board created successfully' });
	} catch (e) {
		if (e.status === 400) {
			res.status(e.status).json({ message: e.message });
		} else {
			return res
				.status(500)
				.json({ message: 'Create Load: Internal server error' });
		}
	}
};

module.exports.getBoard = async (req, res) => {
	const { boardId } = req.params;
	// const {id: userId, role} = req.user;

	if (!boardId) {
		res.status(400).json({
			message: 'You should specify board1 id',
		});
	}

	try {
		// const load = await boardService.getUserBoard(userId, loadId, role);
		const board = await boardService.getUserBoard(boardId);
		res.status(200).json(board);
	} catch (e) {
		if (e.status) {
			res.status(e.status).json({ message: e.message });
		} else {
			return res.status(500).json({ message: 'Internal server error' });
		}
	}
};

module.exports.updateBoard = async (req, res) => {
	const { boardId: boardId } = req.params;
	const payload = req.body;

	try {
		await boardService.updateUserBoard(boardId, payload);
		res.status(200).json({
			message: 'Board details changed successfully',
		});
	} catch (e) {
		if (e.status) {
			res.status(e.status).json({ message: e.message });
		} else {
			res.status(500).json({ message: 'Internal server error' });
		}
	}
};

module.exports.addColumnItem = async (req, res) => {
	console.log('in addBoardColumnItem controller method', req);
	const { boardId: boardId, columnId: columnId } = req.params;
	const payload = req.body;
	console.log('Board ID', boardId);
	console.log('Column ID', columnId);
	console.log('Payload', payload);

	try {
		await boardService.createBoardColumnItem(boardId, payload);
		res.status(200).json({
			message: 'Board details changed successfully 111',
		});
	} catch (e) {
		if (e.status) {
			res.status(e.status).json({ message: e.message });
		} else {
			res.status(500).json({ message: 'Internal server error' });
		}
	}
};

module.exports.deleteBoard = async (req, res) => {
	const { boardId } = req.params;
	// const {id: userId} = req.user;

	if (!boardId) {
		res.status(400).json({
			message: 'You should specify Id of the board1',
		});
	}

	try {
		// await boardService.deleteBoard(userId, loadId);
		await boardService.archiveBoard(boardId);
		res.status(200).json({
			message: 'Board deleted successfully',
		});
	} catch (e) {
		if (e.status) {
			res.status(e.status).json({ message: e.message });
		} else {
			return res.status(500).json({ message: 'Internal server error' });
		}
	}
};
