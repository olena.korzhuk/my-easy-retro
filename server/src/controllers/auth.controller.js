const User = require('./../models/User');
const bcrypt = require('bcryptjs');
const tokenService = require('./../services/token.service');

module.exports.register = async (req, res, next) => {
	const { email, password } = req.body;

	try {
		const sameUser = await User.findOne({ email });
		if (sameUser) {
			return res.status(400).json({
				message: 'User with the same email already exists',
			});
		} else {
			try {
				await User.create({
					email,
					password,
				});
				res.status(200).json({
					message: 'Profile created successfully',
				});
			} catch (e) {
				console.error('REGISTER ERROR', e);
				return next(e);
			}
		}
	} catch (e) {
		return res.status(500).send({
			message: 'Internal Server Error',
		});
	}
};

module.exports.login = async (req, res, next) => {
	const { email, password } = req.body;

	try {
		const user = await User.findOne({ email }).select('+password');

		if (!user) {
			return res.status('500').json({
				message: 'User is not found',
			});
		}

		const isValidPassword = await bcrypt.compare(password, user.password);

		if (!isValidPassword) {
			return res.status(400).json({
				message: 'Incorrect email or password',
			});
		}

		const token = tokenService.generateToken({
			userName: user.email,
			id: user._id,
		});
		res.status(200).json({
			email: user.email,
			jwt_token: token,
		});
	} catch (e) {
		return res.status(500).send({
			message: 'Internal Server Error',
		});
	}
};
