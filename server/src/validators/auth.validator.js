const Joi = require('joi');

const registerSchema = Joi.object({
	userName: Joi.string().min(1).required(),
	email: Joi.string().email().required(),
	password: Joi.string()
		.pattern(new RegExp('[a-zA-Z0-9!@#$%^&*]'))
		.min(1)
		.required(),
});

const loginSchema = Joi.object({
	email: Joi.string().email().required(),
	password: Joi.string()
		.pattern(new RegExp('[a-zA-Z0-9!@#$%^&*]'))
		.min(1)
		.required(),
});

module.exports = {
	registerSchema,
	loginSchema,
};
